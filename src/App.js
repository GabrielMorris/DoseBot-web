// React
import React, { Component } from 'react';

// Components
import Navbar from './components/navbar/Navbar';
import Search from './components/search/Search';
import Results from './components/results/Results';

// Styles
import './normalize.css';
import './skeleton.css';
import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        <Navbar />

        <main className="container body">
          {/* Search row */}
          <div className="row centered">
            <div className="twelve columns">
              <Search />
            </div>
          </div>
          {/* End search row */}

          {/* Results row */}
          <div className="row centered">
            <div className="twelve columns">
              <Results />
            </div>
          </div>
          {/* End results row */}
        </main>
      </div>
    );
  }
}

export default App;
