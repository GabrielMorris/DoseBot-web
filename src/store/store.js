// Redux/thunk
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunk from 'redux-thunk';

// App reducers
import { searchReducer } from '../reducers/search-reducer';

// Redux form
import { reducer as formReducer } from 'redux-form';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(
  combineReducers({
    form: formReducer,
    search: searchReducer
  }),
  composeEnhancers(applyMiddleware(thunk))
);

// export default createStore(formReducer, applyMiddleware(thunk));
