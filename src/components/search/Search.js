// React
import React from 'react';
import { reduxForm, Field } from 'redux-form';

// Actions
import { fetchSearchResults } from '../../actions/search-actions';

export class SearchForm extends React.Component {
  onSubmit(values) {
    this.props.dispatch(fetchSearchResults(values.searchInput));
  }

  render() {
    return (
      <section>
        <form
          className="search-form"
          onSubmit={event => {
            event.preventDefault();
            this.onSubmit(event);
          }}
          role="search"
        >
          {/* Make this invisible but keep it for a11y */}
          {/* <label htmlFor="search-input">Search</label> */}
          <Field
            name="search-input"
            type="text"
            placeholder="Enter your search"
            component="input"
          />

          {/* <button type="submit">Submit</button> */}
        </form>
      </section>
    );
  }
}

export default reduxForm({
  form: 'search'
})(SearchForm);
