// Actions
import { SEARCH_REQUEST } from '../actions/search-actions';

// Initial state
export const initialState = {
  searchResults: [],
  searchTerm: '',
  error: null,
  loading: null
};

export function searchReducer(state = initialState, action) {
  switch (action.type) {
    case SEARCH_REQUEST: {
      console.log(1);
      return Object.assign({}, state, {});
    }

    default: {
      return state;
    }
  }
}
