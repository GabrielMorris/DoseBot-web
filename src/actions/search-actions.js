/* Fetching search results */
const rp = require('request-promise');
const { request } = require('graphql-request');
const infoQuery = require('../include/queries/info');

export const fetchSearchResults = searchTerm => dispatch => {
  dispatch(searchRequest());

  // request('https://api.psychonautwiki.org', infoQuery.info(searchTerm)).then(
  //   data => {
  //     console.log(data);
  //   }
  // );
};

export const SEARCH_REQUEST = 'SEARCH_REQUEST';
export const searchRequest = () => ({
  type: SEARCH_REQUEST
});

export const SEARCH_SUCCESS = 'SEARCH_SUCCESS';
export const searchSuccess = result => ({
  type: SEARCH_SUCCESS,
  result
});

export const SEARCH_ERROR = 'SEARCH_ERROR';
export const searchError = error => ({
  type: SEARCH_ERROR,
  error
});
